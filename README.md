[![Language - Javascript](https://img.shields.io/badge/Javascript-F2DB3F?style=for-the-badge&logo=javascript&logoColor=white)](https://)
[![Plateforme - Docker](https://img.shields.io/badge/Docker-blue?style=for-the-badge&logo=docker&logoColor=white)](https://)



<br/>
<br/>
<div align="center">
    <img src="./logo.png" alt="Logo" width="50%">
    <br/>
    <br/>
    <h1 align="center"><strong>Formation Cypress - Docker</strong></h1>
    <br />
    <p align="center">Docker - Page Object - Actions - CI - Visual Regression</p>
</div>
  
 
<br/>
<br/>
