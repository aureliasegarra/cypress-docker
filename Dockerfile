FROM cypress/base:16.17.0 
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN npm install
CMD ["npm", "run", "cy:run"]